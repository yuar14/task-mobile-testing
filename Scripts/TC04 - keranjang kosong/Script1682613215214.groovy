import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('C:\\Users\\Hello\\Downloads\\Belajar Bootcamp QA\\tugas advance\\Android-MyDemoAppRN.1.2.0.build-231.apk', 
    true)

Mobile.tap(findTestObject('menu'), 0)

Mobile.tap(findTestObject('menu-login'), 0)

Mobile.setText(findTestObject('field username'), 'bob@example.com', 0)

Mobile.setText(findTestObject('field password'), '10203040', 0)

Mobile.tap(findTestObject('tombol_login'), 0)

Mobile.verifyElementText(findTestObject('text_products'), 'Products')

Mobile.tap(findTestObject('gambar_keranjang_halaman_product'), 0)

Mobile.verifyElementText(findTestObject('text_item_kosong'), 'No Items')

Mobile.closeApplication()

